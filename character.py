# -*- coding: utf-8 -*-

# Limbs
def bodyPartToLanguage(part, case = 'nominative'):
    if case == 'nominative':
        if part == 'LeftArm': return "lewa ręka"
        elif part == 'RightArm': return "prawa ręka"
        elif part == 'LeftLeg': return "lewa noga"
        elif part == 'RightLeg': return "prawa noga"
        elif part == "Rorso": return "tors"
        elif part == "Head": return "głowa"
    elif case == 'accusative':
        if part == 'LeftArm': return "lewą rękę"
        elif part == 'RightArm': return "prawą rękę"
        elif part == 'LeftLeg': return "lewą nogę"
        elif part == 'RightLeg': return "prawą nogę"
        elif part == "Torso": return "tors"
        elif part == "Head": return "głowę"
    return None

def languageToBodyPart(part):
    if part in ('lewa ręka', 'lewą rękę'): return "LeftArm"
    elif part in ('prawa ręka', 'prawą rękę'): return "RightArm"
    elif part in ('lewa noga', 'lewą nogę'): return "LeftLeg"
    elif part in ('prawa noga', 'prawą nogę'): return "RightLeg"
    elif part in ('głowa', 'głowę', 'łeb'): return "Head"
    elif part == 'tors': return "Torso"
    return None


# Object's possible characteristics.
def handed(func):
    def wrapper(self, *args):
        self.hands = [None, None]
        func(self, *args)
    wrapper.__doc__ = func.__doc__
    return wrapper
def humanoid(hp, bodyPartInfo):
    def _humanoid(func):
        def wrapper(self, *args):
            self.hp = hp
            self.hpLeftArm = bodyPartInfo['leftArm'][0]
            self.hitPercentLeftArm = bodyPartInfo['leftArm'][1]
            self.hitMultiplierLeftArm = bodyPartInfo['leftArm'][2]
            self.hpRightArm = bodyPartInfo['rightArm'][0]
            self.hitPercentRightArm =  bodyPartInfo['rightArm'][1]
            self.hitMultiplierRightArm =  bodyPartInfo['rightArm'][2]
            self.__class__.hasArms = property(lambda self: self.hpLeftArm > 0 and self.hpRightArm > 0)

            self.hpLeftLeg = bodyPartInfo['leftLeg'][0]
            self.hitPercentLeftLeg = bodyPartInfo['leftLeg'][1]
            self.hitMultiplierLeftLeg = bodyPartInfo['leftLeg'][2]
            self.hpRightLeg = bodyPartInfo['rightLeg'][0]
            self.hitPercentRightLeg = bodyPartInfo['rightLeg'][1]
            self.hitMultiplierRightLeg = bodyPartInfo['rightLeg'][2]
            self.__class__.hasLegs = property(lambda self: self.hpLeftLeg > 0 and self.hpRightLeg > 0)

            self.__class__.hasLimbs = property(lambda self: self.hpLeftLeg > 0 and self.hpRightLeg > 0
                                                        and self.hpLeftArm > 0 and self.hpRightArm > 0)

            self.hpHead = bodyPartInfo['head'][0]
            self.hitPercentHead = bodyPartInfo['head'][1]
            self.hitMultiplierHead = bodyPartInfo['head'][2]
            self.hpTorso = bodyPartInfo['torso'][0]
            self.hitPercentTorso = bodyPartInfo['torso'][1]
            self.hitMultiplierTorso = bodyPartInfo['torso'][2]

            func(self, *args)
        wrapper.__doc__ = func.__doc__
        return wrapper
    return _humanoid
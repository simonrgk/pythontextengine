# -*- coding: utf-8 -*-

import re

class Object(object):
    def __init__(self, owner=None):
        self.owner = owner
        self.characteristic = ["object"]
        self._patterns = [(name, eval("self.%s.pattern" % name)) for name in dir(self) if name.startswith("command_")]
        self._patterns = [(name, pattern.replace("my accusative",
                                                                                 "(%s)" % self.get_names().replace(' ',
                                                                                                                  ')? ?(')))
                          for name, pattern in self._patterns]
        print self._patterns

    def get_names(self):
        return "NN lol"
    def get_name(self):
        return "NN"
    def get_desc(self):
        return "No desc for u"

    def can_parse(self, text):
        for _, pattern in self._patterns:
            if re.match(pattern, text):
                return True
        return False

    def act(self, text):
        for func, pattern in self._patterns:
            match = re.match(pattern, text)
            if match:
                self.matched = match.groupdict()
                #print self.matched
                return getattr(self, func)()
                # Never should be here.

    @staticmethod
    def failure(text):
        print text
        raise "failure"

    def abort_action(self, command):
        global current_messages
        cr.current_messages = filter(lambda x: x[0] != command and x[1] != self, cr.current_messages)


def command(func):
    def wrapper(self, *args):
        try:
            res = func(self, *args)
            return "tooktime" if res == None else res
        except:
            return "failure"

    wrapper.pattern = func.__doc__ + "( )*$"
    return wrapper

# General command modifiers.
def freemove(func):
    def wrapper(self, *args):
        result = func(self, *args)
        return "freemove" if result == None else result
    wrapper.__doc__ = func.__doc__
    return wrapper


# Specific command modifiers.
def singlehanded_draw(func):
    def wrapper(self, *args):
        if self in self.owner.hands:
            print "Już w ręce."
            return 'failure'
        elif None not in self.owner.hands:
            print "Obie ręce są zajęte."
            return 'failure'
        else:
            if self.owner.hands[0] == None:
                self.owner.hands[0] = self
            else:
                self.owner.hands[1] = self
            return func(self, *args)

    wrapper.__doc__ = func.__doc__
    return wrapper
def notnull(sth, failure_message):
    def _notnull(func):
        def wrapper(self, *args):
            if not self.matched[sth]:
                self.failure(failure_message)
            return func(self, *args)
        wrapper.__doc__ = func.__doc__
        return wrapper
    return _notnull
def unambiguous_target(sth, wrong_message, ambiguous_message):
    def _unambiguous_target(func):
        def wrapper(self, *args):
            self._target = cr.get_character(self.matched[sth])
            if len(self._target) != 1:
                if self.matched[sth]:
                    self.failure(wrong_message)
                self.failure(ambiguous_message)
            return func(self, *args)
        wrapper.__doc__ = func.__doc__
        return wrapper
    return _unambiguous_target
def condition(cond, failure_message):
    def _condition(func):
        def wrapper(self, *args):
            if not eval(cond):
                self.failure(failure_message)
            return func(self, *args)
        wrapper.__doc__ = func.__doc__
        return wrapper
    return _condition
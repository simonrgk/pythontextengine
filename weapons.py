# -*- coding: utf-8 -*-

from object import *
from character import *
from random import randint

def isHit(base, modifier, added=0, range=100):
    return (base*modifier/range)+added <= randint(0,range)

def shoot_gun(who, target, part="unspecified", modifiers = (100,0)):
    """
    ('shot', who, target, 5-9, part_of_the_body or "unspecified", (modifier, added))
    """
    cr.next_messages.append([("shot", who, target, randint(5, 9), part, modifiers), 1])

class Gun(Object):
    def __init__(self, owner):
        super(Gun, self).__init__(owner)
        self.bullets = 8

    def get_names(self):
        return "pistolet"
    def get_name(self, case='nominative'):
        if case in ('nominative','accusative'):
            return "pistolet"

    @command
    @singlehanded_draw
    def command_draw(self):
        "wyciagnij ?my accusative?"
        print "Wyciągasz w mgnieniu oka pistolet z kabury"

    @command
    @condition("self in self.owner.hands", "Przed strzałem musisz wyciągnąć pistolet.")
    @notnull('sth', "W kogo?")
    @unambiguous_target('sth', "Zły cel.", "Sprecyzuj cel.")
    def command_shoot(self):
        "strzel( w)?(?P<sth>(.*?))?( w (?P<part>(.*)))?"
        part = self.matched['part']
        body_part = "unspecified"
        if part:
            body_part = languageToBodyPart(part)
            if not body_part: self.failure("Zła część ciała.")
        print "Wypalasz w %s." % bodyPartToLanguage(body_part, "accusative")
        shoot_gun(self.owner, self._target[0], body_part)

    def act(self, text):
        return super(Gun, self).act(text)


def punch(who, target, message = ""):
    cr.broadcast(("punch", who, target, randint(1, 2), message))

class NormalFists(Object):
    def __init__(self, owner):
        super(NormalFists, self).__init__(owner)

    def get_names(self):
        return "pięść pięści"
    def get_name(self, case = 'nominative'):
        if case in ('nominative','accusative'):
            return "pięść"

    @command
    @notnull('sth', "Kogo?")
    @unambiguous_target('sth', "Zły cel.", "Sprecyzuj cel.")
    def command_punch(self):
        "uderz( (?P<sth>(.*)))?"
        print "Uderzasz %s." % self._target[0].get_name('accusative')
        punch(self.owner, self._target[0])

    def act(self, text):
        return super(NormalFists, self).act(text)

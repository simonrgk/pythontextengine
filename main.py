# -*- coding: utf-8 -*-

from room import Room, set_room

from character import *
from weapons import *


class PlayerAI(Object):
    @handed
    @humanoid(10, {'leftArm':(5,75,1),'rightArm':(5,75,1),'leftLeg':(5,75,1),'rightLeg':(5,75,1),'head':(1,60,10),'torso':(10,95,1)})
    def __init__(self):
        super(PlayerAI, self).__init__(None)
        fists = NormalFists(self)
        self.inventory = [Gun(self), fists]
        self.hands = [None, fists]
    def get_names(self):
        return "sobie siebie"
    def get_name(self, case = "nominative"):
        if case == 'nominative':
            return "ty"
        elif case == 'accusative':
            return "ciebie"
        elif case == 'genitive':
            return "tobie"
    def get_desc(self):
        desc = "Oto ty."
        if self.hp == 10:
            desc += " Jesteś w całkiem niezłej kondycji."
        elif 10 > self.hp > 7:
            desc += " Jesteś lekko pokiereszowany."
        elif 7 > self.hp > 4:
            desc += " Czujesz się słabo."
        elif 4 > self.hp > 0:
            desc += " Zaczynasz się godzić z wizją nieuniknionej śmierci."
        lost_parts = []
        if self.hpLeftArm <= 0: lost_parts.append("lewej ręki")
        if self.hpRightArm <= 0: lost_parts.append("prawej ręki")
        if self.hpLeftArm <= 0: lost_parts.append("lewej nogi")
        if self.hpRightArm <= 0: lost_parts.append("prawej nogi")
        if lost_parts:
            desc += " Nie masz tylko %s." % lost_parts[0]
            for part in lost_parts[1:]:
                desc += "\nI %s." % part
        return desc

    @command
    def command_wait(self):
        "(czekaj)|(nic nie rób)"
        print "Czekasz."

    @command
    @freemove
    def command_examine(self):
        "spojrz( na (?P<sth>(.*?)))?"
        if not self.matched['sth']:
            self.failure("Na kogo?")
        else:
            target = cr.get_character(self.matched['sth'])
            if len(target) != 1:
                if self.matched['sth']:
                    self.failure("Nie widzisz niczego takiego w pobliżu.")
                self.failure("Na kogo dokładnie?")
            print target[0].get_desc()

    def act(self):
        result = "failure"
        while result != "tooktime":
            text = raw_input("(akcja) >> ")
            if self.can_parse(text):
                result = super(PlayerAI, self).act(text) # Firsy try to parse command by your own.
                continue
            possibleHandlers = [] # Then pass control to items in your inventory.
            for item in self.inventory:
                if item.can_parse(text): possibleHandlers.append(item)
            if not possibleHandlers:
                print "Nie rozumiem."
            elif len(possibleHandlers) == 1:
                result = possibleHandlers[0].act(text)
            else:
                print "Wiele możliwych interpretacji."

    def parse_messages(self):
        for message, life in cr.current_messages:
            if message[0] == 'shot':
                # TODO: Implement other responses.
                if message[1] == message[2] == self:
                    part = message[4]
                    body_part = bodyPartToLanguage(part)
                    if part == 'head':
                        print "Nie wytrzymując wywieranej przez Wszechświat presji przykładasz lufę do swojej skroni i naciskasz spust."
                        self.hp = 0
                    elif part:
                        print "Po szybkiej analizie swojej sytuacji stwierdzasz, że %s jest tylko niepotrzebnym balastem." % body_part
                        exec "self.hp%s = 0" % part
                    else:
                        print "Postanawiasz ukarać się za grzechy twojego dzieciństwa."
                    self.hp -= message[3]
                    break
            elif message[0] == 'punch':
                if message[2] == self:
                    name = message[1].get_name().capitalize()
                    print message[4]
                    part = randint(0, 2)
                    if part == 0 and self.hpLeftArm > 0:
                        print "%s trafia cię pięścią w lewą rękę. Północne wiatry ci nie sprzyjają i uszkodzona ręka eksploduje niczym w tanim hollywoodzkim gniocie.\nInteresujące." % name
                        self.hpLeftArm = 0
                    elif part == 1 and self.hpRightLeg > 0:
                        print "%s trafia cię pięścią w prawą nogę, która magicznie odpada. Wow." % name
                        self.hpRightLeg = 0
                    elif part == 2 and self.hpLeftLeg > 0:
                        print "%s trafia cię pięścią w lewą nogę. Gniew boży powoduje jej nagłą dezintegrację." % name
                        self.hpLeftLeg = 0
                    else:
                        print "%s trafia cię pięścią w tors." % name
                    self.hp -= message[3]


class CyborgGruntAI(Object):
    @handed
    @humanoid(10, {'leftArm':(8,85,1),'rightArm':(8,85,1),'leftLeg':(8,85,1),'rightLeg':(8,85,1),'head':(1,60,10),'torso':(20,95,1)})
    def __init__(self):
        super(CyborgGruntAI, self).__init__(None)
        self.target = None

    def get_names(self):
        return "cyborg cyborga cyborgowi"
    def get_name(self, case = "nominative"):
        if case == 'nominative':
            return "cyborg"
        elif case == 'accusative':
            return "cyborga"
        elif case == 'genitive':
            return "cyborgowi"

    def get_desc(self):
        desc = "Będąca do niedawna członkiem załogi mieszanina ludzkiej tkanki i metalu przeszywa cię swoim świecącym karmazynowymi ślepiami. Nie potrafisz ocenić stanu zdrowia tej abominacji."
        if self.hpLeftArm <= 0 and self.hpRightArm <= 0 and self.hpLeftArm <= 0 and self.hpRightArm <= 0:
            desc += "\nCyborg pozbawiony rąk i nóg nie stanowi większego zagrożenia."
        else:
            if self.hpLeftArm <= 0 and self.hpRightArm <= 0:
                desc += "\nCyborg został pozbawiony obu rąk, co znacząco zmniejsza zagrożenie z jego strony."
            else:
                if self.hpLeftArm <= 0:
                    desc += "\nCyborgowi brakuje lewej ręki."
                elif self.hpRightArm <= 0:
                    desc += "\nCyborgowi brakuje prawej ręki."
            if self.hpLeftArm <= 0 and self.hpRightArm <= 0:
                desc += " Cyborgowi brakuje dolnej części ciała."
            else:
                if self.hpLeftLeg <= 0:
                    desc += "\nCyborgowi brakuje lewej nogi."
                elif self.hpRightLeg <= 0:
                    desc += "\nCyborgowi brakuje prawej nogi."
        return desc

    def act(self):
        # TODO: Attacking other rogues.
        if not self.target:
            self.target = cr.get_random_character(self)
            if self.target == cr.player:
                print "Cyborg wlepia w ciebie swoje mechaniczne oczy."
            else:
                print "Cyborg cię nie zauważa."
                self.target = None
                return
        action = randint(0, 4)
        if action == 0:
            print "Cyborg stoi w milczeniu."
        else:
            if self.hpLeftArm > 0 or self.hpRightArm > 0:
                print "Cyborg niezdarnie próbuje uderzyć cię pięścią."
                punch(self, self.target, "Widzisz lecącą w twoim kierunku pięść przeznaczenia.")
            else:
                print "Cyborg nerwowo kręci głową patrząc na kikuty swoich rąk."


    def parse_messages(self):
        for message, life in cr.current_messages:
            if message[0] == 'shot':
                if message[2] == self:
                    part = message[4]
                    modifier, added = message[5]
                    damage = message[3]
                    if part == 'unspecified':
                        print "Kula trafia cyborga w tors."
                        self.hp -= damage
                        self.abort_action("punch")
                    elif part == 'Head':
                        if isHit(self.hitPercentHead, modifier, added):
                            print "Kula przeszywa głowę cyborga. Czerwone ślepia gasną."
                            cr.characters.remove(self)
                            self.abort_action("punch")
                        else:
                            print "Pocisk mija głowę cyborga."
                    else:
                        hp = eval("self.hp%s" % part)
                        percent = eval("self.hitPercent%s" % part)
                        if hp <= 0:
                            print "Kula przelatuje przez nieistniejącą kończynę."
                        else:
                            if isHit(eval("self.hitPercent%s" % part), modifier, added):
                                exec "self.hp%s -= %s" % (part, damage)
                                self.hp -= damage*eval("self.hitMultiplier%s" % part)
                                self.abort_action("punch")
                                if eval("self.hp%s" % part) <= 0:
                                    print "Kula przelatuje przez %s odrywając ją." % bodyPartToLanguage(part, 'accusative')
                                else:
                                    print "Kula trafia %s cyborga." % bodyPartToLanguage(part, 'accusative')
                            else:
                                print "Kula mija %s cyborga." % bodyPartToLanguage(part, 'accusative')
        if self.hp <= 0:
            print "Cyborg wybucha w iście filmowym stylu na tle twojej majestatycznej sylwetki."
            cr.characters.remove(self)

set_room(Room(PlayerAI()))
cr.add_character(CyborgGruntAI())
def death():
    print "Gratulacje, przegrałeś."
cr.start(death)
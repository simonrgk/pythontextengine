# -*- coding: utf-8 -*-

from random import choice

import __builtin__
__builtin__.cr = None
def set_room(room):
    __builtin__.cr = room

class Room(object):
    def __init__(self, player):
        self.characters = []
        self.player = player
        self.add_character(player)
        self.next_messages = []
        self.current_messages = []

    def broadcast(self, message, life = 1):
        self.next_messages.append([message, life])

    def add_character(self, character):
        self.characters.append(character)
    def get_character(self, name):
        possible_characters = []
        for character in self.characters:
            if set(character.get_names().split()).issuperset(set(name.split())):
                possible_characters.append(character)
        return possible_characters
    def get_random_character(self, *args):
        return choice(list(set(self.characters) - set(args)))

    def start(self, deathFunc):
        while True:
            for character in self.characters:
                character.act()
            self.current_messages = self.next_messages[:]
            self.next_messages = filter(lambda x: x[1] != 0, map(lambda x: [x[0], x[1] - 1], self.next_messages))
            #print current_messages
            for character in self.characters:
                character.parse_messages()
            if self.player.hp <= 0:
                deathFunc()
                break